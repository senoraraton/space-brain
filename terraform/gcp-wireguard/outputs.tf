output "username" {
  value = split("@", data.google_client_openid_userinfo.iac_vpc_user.email)[0]
}
output "private_key" {
  value     = tls_private_key.iac_vpn_ssh_keys.private_key_pem
  sensitive = true
}
output "instance_public_ip" {
  value = google_compute_instance.iac_vpn_instance.network_interface[0].access_config[0].nat_ip
}
