data "local_file" "ssh_public_key" {
    filename = "/home/senoraraton/.ssh/spaceshipkey.pub"
}

data "oci_core_images" "nfs_image" {
    compartment_id = var.compartment_ocid
    display_name = "nfs_image_node"
}

resource "oci_core_instance" "nfs-node" {
    count = 2

    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name

    compartment_id      = var.compartment_ocid
    display_name        = "Nfs_node_${count.index + 1}"
    shape               = var.x86_64_node_shape
    
  create_vnic_details {
    subnet_id        = oci_core_subnet.vcn_nfs_node_subnet.id
    display_name     = "nfs_node_${count.index + 1}-vnic"
    assign_public_ip = false
  }

  source_details {
    source_type = "image"
    source_id   = data.oci_core_images.nfs_image.images[0].id
    }

  metadata = {
    ssh_authorized_keys = (data.local_file.ssh_public_key.content != "") ? data.local_file.ssh_public_key.content : tls_private_key.compute_ssh_key.public_key_openssh
  }
}


resource "tls_private_key" "compute_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

output "generated_private_key_pem" {
  value     = (var.ssh_public_key != "") ? var.ssh_public_key : tls_private_key.compute_ssh_key.private_key_pem
  sensitive = true
}
