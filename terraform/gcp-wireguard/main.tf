provider "google" {
  project = var.gcp-wireguard-vpn
  region  = var.gcp-wireguard-region
  zone    = var.gcp-wireguard-zone
}

data "google_client_openid_userinfo" "iac_vpc_user" {}

resource "tls_private_key" "iac_vpn_ssh_keys" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "google_compute_network" "iac_vpn_network" {
    name = "iac-vpn-network"
    auto_create_subnetworks = false
}

resource "null_resource" "save_private_key" {
  triggers = {
    private_key = tls_private_key.iac_vpn_ssh_keys.private_key_pem
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo '${tls_private_key.iac_vpn_ssh_keys.private_key_pem}' > private_key.pem
    EOT
  }
}

resource "google_compute_subnetwork" "iac_vpc_subnet" {
    name = "iac-vpn-subnet"
    ip_cidr_range = var.gcp-wireguard-subnet-cidr
    region = var.gcp-wireguard-region
    network = google_compute_network.iac_vpn_network.id
}

resource "google_compute_firewall" "iac_vpn_firewall" {
    name = "iac-vpn-firewall"
    allow {
        ports = ["22", "6443"]
        protocol = "tcp"
    }
    allow {
        ports = [var.gcp-wireguard-port]
        protocol = "udp"
    }
    direction = "INGRESS"
    network = google_compute_network.iac_vpn_network.id
    priority = 1000
    source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_address" "iac_vpn_static_ip" {
    name = "iac-vpn-static-ip"
    region = var.gcp-wireguard-region
}

resource "google_compute_instance" "iac_vpn_instance" {
    name = "iac-vpn-instance"
    machine_type = var.gcp-wireguard-machine-type
    
    boot_disk {
        initialize_params {
            image = var.gcp-wireguard-image
        }
    }
    
    network_interface {
        subnetwork = google_compute_subnetwork.iac_vpc_subnet.id
        access_config {
            nat_ip = google_compute_address.iac_vpn_static_ip.address
        }
    }

    metadata = {
        ssh-keys = "${split("@", data.google_client_openid_userinfo.iac_vpc_user.email)[0]}:${tls_private_key.iac_vpn_ssh_keys.public_key_openssh}"
    }
}
