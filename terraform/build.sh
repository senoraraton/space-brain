#!/usr/bin/env sh
#Build NFS node image
cd ./nfs-host-image || exit
terraform apply -auto-approve
terraform destroy -auto-approve

#Bring up the cluster
cd ../oke-cluster || exit
terraform apply -auto-approve

#Clean-up NFS image
image_ocid=$(oci compute image list --compartment-id ocid1.tenancy.oc1..aaaaaaaatltxhoeu6yzjpetw7io22iwd6rn7wujmgybqje5pqvpkdidkwdea --display-name nfs_image_node | jq -r '.data[0].id')
oci compute image delete --image-id "$image_ocid" --force
