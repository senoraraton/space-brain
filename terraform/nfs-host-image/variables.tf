variable "ssh_public_key" {
  default = "/home/senoraraton/.ssh/spaceshipkey.pub"
}

variable "ssh_private_key" {
    type = string
    default = "/home/senoraraton/.ssh/spaceshipkey"
}

variable "tenancy_ocid" {
    type = string
    default = "ocid1.tenancy.oc1..aaaaaaaatltxhoeu6yzjpetw7io22iwd6rn7wujmgybqje5pqvpkdidkwdea"
}
variable "user_ocid" {
    type = string
    default = "ocid1.user.oc1..aaaaaaaawqt3lzu3xwlzm6vtrnjj6tt5ux7wyyiha3jqgz5j3gz4szdnl7yq"
}
variable "compartment_ocid" {
    type = string
    default = "ocid1.tenancy.oc1..aaaaaaaatltxhoeu6yzjpetw7io22iwd6rn7wujmgybqje5pqvpkdidkwdea"
}
variable "fingerprint" {
    type = string
    default="46:c1:30:f5:00:ed:29:04:09:58:85:72:98:08:61:43"
}
variable "oci_region" {
    type = string
    default = "us-sanjose-1"
}

variable "x86_64_node_shape" {
    type = string
    default = "VM.Standard.E2.1.Micro"
}
variable "nfs_node_subnet" {
    type = string
    default = "10.0.3.0/24"
}
variable "k8s_node_subnet" {
    type = string
    default = "10.0.2.0/24"
}
