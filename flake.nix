{
  description = "Claus' Portfolio Web Site";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfreePredicate = pkg: true;
    };
  in {
    devShells.x86_64-linux = {
      default = pkgs.mkShell {
        buildInputs = with pkgs; [
          ansible
          yamllint
          nodePackages.prettier
          python311Packages.docker
          terraform
          terraform-providers.oci
          oci-cli
          google-cloud-sdk
          kubernetes-helm
          kubectl
          openssl
          wrk
          cmctl
          certbot
          packer
        ];
        shellHook = ''
          export GOOGLE_APPLICATION_CREDENTIALS=/home/senoraraton/.config/gcloud/application_default_credentials.json
        '';
      };
    };
  };
}
