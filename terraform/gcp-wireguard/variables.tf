variable "gcp-wireguard-vpn" {
    type = string
    description = "Project ID of the GCP-Wireguard VPN"
    default = "714242255670"
}
variable "gcp-wireguard-region" {
    type = string
    description = "GCP region to deploy VPN to"
    default = "us-west1"
}
variable "gcp-wireguard-zone" {
    type = string
    description = "GCP zone to deploy VPN to"
    default = "us-west1-a"
}
variable "gcp-wireguard-machine-type" {
    type = string
    description = "Machine type for instance"
    default = "e2-micro"
}
variable "gcp-wireguard-image" {
    type = string
    description = "Image for VPN"
    default = "ubuntu-os-cloud/ubuntu-2204-lts"
}
variable "gcp-wireguard-subnet-cidr" {
    type = string
    description = "CIDR of subnet"
    default = "10.0.0.0/24"
}
variable "gcp-wireguard-port" {
    type = string
    description = "Port for Wireguard"
    default = "51820"
}
