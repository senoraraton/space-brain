data "local_file" "ssh_public_key" {
    filename = "/home/senoraraton/.ssh/spaceshipkey.pub"
}

data "oci_identity_availability_domains" "ad" {                                                                                       
    compartment_id = var.compartment_ocid                                                                                                    
}                                                                                                                                              

resource "tls_private_key" "compute_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

output "generated_private_key_pem" {
  value     = (var.ssh_public_key != "") ? var.ssh_public_key : tls_private_key.compute_ssh_key.private_key_pem
  sensitive = true
}

resource "oci_core_vcn" "nfs_image_vcn" {
    compartment_id = var.compartment_ocid

    cidr_blocks = [ "10.100.0.0/24" ]
    display_name = "temp-nfs-image-vcn"
}

resource "oci_core_internet_gateway" "nfs_image_ig"{
    compartment_id = var.compartment_ocid
    vcn_id = oci_core_vcn.nfs_image_vcn.id

    display_name = "temp-nfs-image-ig"
    enabled = true
}

resource "oci_core_route_table" "nfs_image_rt" {
    compartment_id = var.compartment_ocid
    vcn_id = oci_core_vcn.nfs_image_vcn.id

    display_name = "temp-nfs-image-rt"
    route_rules {
        network_entity_id = oci_core_internet_gateway.nfs_image_ig.id
        destination_type = "CIDR_BLOCK"
        destination = "0.0.0.0/0"
    }

}

resource "oci_core_subnet" "nfs_image_subnet" {
    cidr_block = "10.100.0.0/24"
    compartment_id = var.compartment_ocid
    vcn_id = oci_core_vcn.nfs_image_vcn.id
    route_table_id = oci_core_route_table.nfs_image_rt.id
    display_name = "temp-nfs-image-subnet"
}

resource "oci_core_network_security_group" "nfs_image_sg" {
    compartment_id = var.compartment_ocid
    vcn_id = oci_core_vcn.nfs_image_vcn.id
    display_name = "temp-nfs-image-sg"
}

resource "oci_core_network_security_group_security_rule" "nfs_image_ssh_ingress" {
    description = "Allow ssh ingress to nfs image node"
    network_security_group_id = oci_core_network_security_group.nfs_image_sg.id
    direction = "INGRESS"
    source = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 22
            max = 22
        }
    }
}

resource "oci_core_network_security_group_security_rule" "nfs_image_ssh_egress" {
    description = "Allow ssh ingress to nfs image node"
    network_security_group_id = oci_core_network_security_group.nfs_image_sg.id

    direction = "EGRESS"
    destination = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 22
            max = 22
        }
    }
}

data "oci_core_images" "nfs_builder_images" {
    compartment_id           = var.compartment_ocid
    operating_system         = "Oracle Linux"
    operating_system_version = "8"
    shape                    = var.x86_64_node_shape 
    sort_by                  = "TIMECREATED"
    sort_order               = "DESC"
}

resource "oci_core_instance" "nfs_builder" {
    availability_domain = data.oci_identity_availability_domains.ad.availability_domains[0].name
    compartment_id      = var.compartment_ocid
    display_name        = "nfs_image_node"
    shape               = var.x86_64_node_shape
    
  create_vnic_details {
    subnet_id        = oci_core_subnet.nfs_image_subnet.id
    display_name     = "nfs_image_node_vnic"
    nsg_ids = [ oci_core_network_security_group.nfs_image_sg.id ]
    assign_public_ip = true
  }

  source_details { 
    source_type = "image"
    source_id   = lookup(data.oci_core_images.nfs_builder_images.images[0], "id")
  }

  metadata = {
    ssh_authorized_keys = (data.local_file.ssh_public_key.content != "") ? data.local_file.ssh_public_key.content : tls_private_key.compute_ssh_key.public_key_openssh
  }
    provisioner "remote-exec" {
        inline = ["echo 'Waiting until SSH is ready'"]

        connection {
            type = "ssh"
            user = "opc"
            private_key = file(var.ssh_private_key)
            host = oci_core_instance.nfs_builder.public_ip
        }
    }

    provisioner "local-exec" {
        command = "ansible-playbook -i ${oci_core_instance.nfs_builder.public_ip}, --private-key ${var.ssh_private_key} -e 'ANSIBLE_HOST_KEY_CHECKING=False' -u opc -e 'node_subnet=${var.k8s_node_subnet}' ./nfs-init.yml"
    }
}

resource "null_resource" "create_image" {
    depends_on = [ oci_core_instance.nfs_builder ]

    provisioner "local-exec" {
        command = <<-EOT
        oci compute image create --instance-id ${oci_core_instance.nfs_builder.id} --display-name "nfs_image_node" --compartment-id ${var.compartment_ocid} --wait-for-state AVAILABLE
        EOT
    }
}
