output "k8s_node_subnet" {
    value = var.k8s_node_subnet
}

output "image_name" {
    value = oci_core_instance.nfs_builder.display_name
}
