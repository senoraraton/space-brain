terraform {
  required_providers {
    oci = {
        source = "oracle/oci"
        version = "~> 5.21.0"
    }
    ansible = {
        source  = "ansible/ansible"
        version = "~> 1.1.0"
    }
  }
}
