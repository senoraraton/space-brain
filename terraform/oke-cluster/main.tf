provider "oci" {
  region           = var.oci_region
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.ssh_private_key
}

module "vcn" {
    source = "oracle-terraform-modules/vcn/oci"
    version = "3.6.0"

    compartment_id = var.compartment_ocid
    region = var.oci_region

    create_internet_gateway = true
    create_nat_gateway = true
    create_service_gateway = true

    vcn_name = "spacedev-k8s-vcn"
    vcn_dns_label = "spacedevk8svcn"
    vcn_cidrs = ["10.0.0.0/16"]

}

resource "oci_core_subnet" "vcn_kubeapi_subnet" {                                                                                               
  compartment_id    = var.compartment_ocid                                                                                                       
  vcn_id            = module.vcn.vcn_id                                                                                                        
  cidr_block        = "10.0.0.0/29"                                                                                                            
  route_table_id    = module.vcn.ig_route_id                                                                                                   
  display_name      = "spacedev-kubeapi-subnet"                                                                                                 
}

resource "oci_core_subnet" "vcn_lb_subnet" {
  compartment_id    = var.compartment_ocid                                                                                                       
  vcn_id            = module.vcn.vcn_id                                                                                                        
  cidr_block        = "10.0.1.0/24"                                                                                                            
  route_table_id    = module.vcn.ig_route_id
  display_name      = "spacedev-lb-subnet"                                                                                                 
}
                                                                                                                                               
resource "oci_core_subnet" "vcn_node_subnet" {                                                                                              
  compartment_id             = var.compartment_ocid                                                                                              
  vcn_id                     = module.vcn.vcn_id                                                                                               
  cidr_block                 = "10.0.2.0/24"                                                                                                   
  route_table_id             = module.vcn.nat_route_id                                                                                         
  display_name               = "spacedev-node-subnet"                                                                                       
  prohibit_public_ip_on_vnic = true                                                                                                            
}                                                                                                                                              

resource "oci_core_subnet" "vcn_nfs_node_subnet" {                                                                                              
  compartment_id             = var.compartment_ocid                                                                                              
  vcn_id                     = module.vcn.vcn_id                                                                                               
  cidr_block                 = "10.0.3.0/24"                                                                                                   
  route_table_id             = module.vcn.nat_route_id                                                                                         
  display_name               = "spacedev-nfs-subnet"                                                                                       
  prohibit_public_ip_on_vnic = true                                                                                                            
}                                                                                                                                              
