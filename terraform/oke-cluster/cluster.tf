#https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengnetworkconfigexample.htm

resource "oci_core_network_security_group" "k8s_apiserver" {
    compartment_id = var.compartment_ocid
    vcn_id = module.vcn.vcn_id
    display_name = "oke-apisever"
}

#Public Kubernetes API security rules

resource "oci_core_network_security_group_security_rule" "ingress_apiserver_workers" {
    description = "Allow worker node communcation to k8s api"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction =  "INGRESS"
    source = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
    destination_port_range {
        min = 6443
        max = 6443
        }
    }
}

resource "oci_core_network_security_group_security_rule" "ingress_apiserver_controlplane" {
    description = "Allow worker node communcation to k8s api"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction = "INGRESS"
    source = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
    destination_port_range {
        min = 12250
        max = 12250
        }
    }
}

resource "oci_core_network_security_group_security_rule" "ingress_apiserver_path_discovery" {
    description = "Allow inbound icmp for MTU"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction = "INGRESS"
    source = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "1"
    icmp_options {
        type = 3
        code = 4
    }
}

resource "oci_core_network_security_group_security_rule" "egress_oke" {
    description = "Allow controlplane comms with OKE"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction = "EGRESS"
    destination_type = "SERVICE_CIDR_BLOCK"
    destination = "all-sjc-services-in-oracle-services-network"
    protocol = "6"
}

resource "oci_core_network_security_group_security_rule" "egress_services_icmp" {
    description = "Allow controlplane path discovery with OKE"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction = "EGRESS"
    destination_type = "SERVICE_CIDR_BLOCK"
    destination = "all-sjc-services-in-oracle-services-network"
    protocol = "1"
    icmp_options {
        type = 3
        code = 4
    }
}

resource "oci_core_network_security_group_security_rule" "control_plane_to_worker_nodes" {
    description = "Allow control plane to communicate with worker nodes"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction = "EGRESS"
    destination = "10.0.2.0/24"
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 1
        max = 65535
        }
    }
}

resource "oci_core_network_security_group_security_rule" "egress_services_icmp_nodes" {
    description = "Allow controlplane path discovery with nodes"
    network_security_group_id = oci_core_network_security_group.k8s_apiserver.id

    direction = "EGRESS"
    destination = "10.0.2.0/24"
    protocol = "1"
    icmp_options {
        type = 3
        code = 4
    }
}

resource "oci_core_network_security_group_security_rule" "Home_kubeapi_access" {                                                            
  description               = "Allow apiserver ingress from HOME"                                                                              
  network_security_group_id = oci_core_network_security_group.k8s_apiserver.id                                                                 
                                                                                                                                               
  direction = "INGRESS"                                                                                                                        
  source    = "192.195.83.130/32"
  protocol  = "6"                                                                                                                              
  tcp_options {                                                                                                                                
    destination_port_range {                                                                                                                   
      min = 6443                                                                                                                               
      max = 6443                                                                                                                               
    }                                                                                                                                          
  }                                                                                                                                            
}

#Private Nodes Security Rules

resource "oci_core_network_security_group" "k8s_nodes" {
    compartment_id = var.compartment_ocid
    vcn_id = module.vcn.vcn_id
    display_name = "oke-nodes"
}

resource "oci_core_network_security_group_security_rule" "pod_to_pod_across_nodes" {
    description = "Allow pods on on node to talk to other pods on other node"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "all"
}

resource "oci_core_network_security_group_security_rule" "control_plane_to_nodes" {
    description = "Allow control plane to comm with nodes"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = oci_core_subnet.vcn_kubeapi_subnet.cidr_block
    protocol = "all"
}

resource "oci_core_network_security_group_security_rule" "ingress_node_icmp" {
    description = "Allow inbound node icmp for MTU"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = "0.0.0.0/0"
    protocol = "1"
    icmp_options {
        type = 3
        code = 4
    }
}

resource "oci_core_network_security_group_security_rule" "ingress_node_lb" {
    description = "Allow Load balancer to node port"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = oci_core_subnet.vcn_lb_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 30000
        max = 32767
        }
    }
}

resource "oci_core_network_security_group_security_rule" "ingress_node_lb_proxy" {
    description = "Allow Load balancer to kubeproxy on node port"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = oci_core_subnet.vcn_lb_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 10256
        max = 10256
        }
    }
}

resource "oci_core_network_security_group_security_rule" "egress_nodes_pods" {
    description = "Allow node communication to pods"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "all"
}

resource "oci_core_network_security_group_security_rule" "egress_node_icmp" {
    description               = "Allow nodes MTU/Path discovery"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction   = "EGRESS"
    destination = "0.0.0.0/0"
    protocol    = "1"
    icmp_options {
        type = 3
        code = 4
    }
}

resource "oci_core_network_security_group_security_rule" "egress_node_oke" {
    description = "Allow nodes to communicate with oke"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination_type = "SERVICE_CIDR_BLOCK"
    destination = "all-sjc-services-in-oracle-services-network"
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 1
        max = 65535
        }
    }
}

resource "oci_core_network_security_group_security_rule" "egress_node_api" {
    description = "Allow node to api comms"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_kubeapi_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 6443
        max = 6443
        }
    }
}


resource "oci_core_network_security_group_security_rule" "egress_node_api2" {
    description = "Allow node to api comms"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_kubeapi_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 12250
        max = 12250
        }
    }
}

resource "oci_core_network_security_group_security_rule" "node_internet" {
    description = "Allow outbound to internet on nodes"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 1
        max = 65535
        }
    }
}


resource "oci_core_network_security_group_security_rule" "node-rpcbind" {
    description = "Allow nodes rpcbind ingress"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = "10.0.3.0/24"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 111
            max = 111
        }
    }
}

resource "oci_core_network_security_group_security_rule" "node-nfs-server" {
    description = "Allow node nfs server ingress"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "INGRESS"
    source = "10.0.3.0/24"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 2049
            max = 2049
        }
    }
}

resource "oci_core_network_security_group_security_rule" "rpc_egress_nodes" {
    description = "Allow node comms with rpc"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_nfs_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 111
            max = 111
        }
    }
}

resource "oci_core_network_security_group_security_rule" "node_egress_nfs_nodes" {
    description = "Allow node comms with nfs server"
    network_security_group_id = oci_core_network_security_group.k8s_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_nfs_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 2049
            max = 2049
        }
    }
}

#Private NFS Nodes Security Rules

resource "oci_core_network_security_group" "nfs_nodes" {
    compartment_id = var.compartment_ocid
    vcn_id = module.vcn.vcn_id
    display_name = "nfs-nodes"
}

resource "oci_core_network_security_group_security_rule" "nfs-rpcbind" {
    description = "Allow nfs rpcbind ingress"
    network_security_group_id = oci_core_network_security_group.nfs_nodes.id

    direction = "INGRESS"
    source = "10.0.2.0/24"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 111
            max = 111
        }
    }
}

resource "oci_core_network_security_group_security_rule" "nfs-server" {
    description = "Allow nfs server ingress"
    network_security_group_id = oci_core_network_security_group.nfs_nodes.id

    direction = "INGRESS"
    source = "10.0.2.0/24"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 2049
            max = 2049
        }
    }
}

resource "oci_core_network_security_group_security_rule" "rpc_egress_nfs_nodes" {
    description = "Allow rpc comms with nodes"
    network_security_group_id = oci_core_network_security_group.nfs_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 111
            max = 111
        }
    }
}

resource "oci_core_network_security_group_security_rule" "nfs_egress_nfs_nodes" {
    description = "Allow nfs server comms with nodes"
    network_security_group_id = oci_core_network_security_group.nfs_nodes.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 2049
            max = 2049
        }
    }
}

#Public LB security list

resource "oci_core_network_security_group" "k8s_lb" {
    compartment_id = var.compartment_ocid
    vcn_id = module.vcn.vcn_id
    display_name = "oke-lb"
}

resource "oci_core_network_security_group_security_rule" "ingress_lb_http" {
    description = "Allow LB HTTP"
    network_security_group_id = oci_core_network_security_group.k8s_lb.id 

    direction = "INGRESS"
    source = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 80
        max = 80
        }
    }
}

resource "oci_core_network_security_group_security_rule" "ingress_lb_https" {
    description = "Allow LB HTTPS"
    network_security_group_id = oci_core_network_security_group.k8s_lb.id 

    direction = "INGRESS"
    source = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 443
        max = 443
        }
    }
}

resource "oci_core_network_security_group_security_rule" "ingres_lb_all" {
    description = "Allow ingress on node ports for LB"
    network_security_group_id = oci_core_network_security_group.k8s_lb.id

    direction = "INGRESS"
    source = "0.0.0.0/0"
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 30000
            max = 32767
        }
    }
}

resource "oci_core_network_security_group_security_rule" "egress_lb_nodes" {
    description = "Allow LB comms to node ports"
    network_security_group_id = oci_core_network_security_group.k8s_lb.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
            min = 30000
            max = 32767
        }
    }
}

resource "oci_core_network_security_group_security_rule" "egress_lb_kubeproxy" {
    description = "Allow LB to communicate with kube proxy on worker nodes"
    network_security_group_id = oci_core_network_security_group.k8s_lb.id

    direction = "EGRESS"
    destination = oci_core_subnet.vcn_node_subnet.cidr_block
    protocol = "6"
    tcp_options {
        destination_port_range {
        min = 10256
        max = 10256
        }
    }
}

resource "oci_containerengine_cluster" "k8s_cluster" {
    compartment_id = var.compartment_ocid
    kubernetes_version = var.k8s_version
    name = "oci-k8s"
    vcn_id = module.vcn.vcn_id

    endpoint_config {
        is_public_ip_enabled = true
        subnet_id = oci_core_subnet.vcn_kubeapi_subnet.id
        nsg_ids = [oci_core_network_security_group.k8s_apiserver.id]
    }

    options {
        add_ons {
            is_kubernetes_dashboard_enabled = false
            is_tiller_enabled = false
        }
        kubernetes_network_config {
            pods_cidr = "10.30.0.0/16"
            services_cidr = "10.96.0.0/16"
        }
        service_lb_subnet_ids = [oci_core_subnet.vcn_lb_subnet.id]
    }
}

data "oci_core_images" "arm_image" {
    compartment_id = var.compartment_ocid

    operating_system         = "Oracle Linux"
    operating_system_version = "8"
    shape                    = var.arm_node_shape
    state                    = "AVAILABLE"
    sort_by                  = "TIMECREATED"
}

data "local_file" "ssh_key" {
    filename = "/home/senoraraton/.ssh/spaceshipkey.pub"
}

data "oci_identity_availability_domains" "ads" {
    compartment_id = var.compartment_ocid
}

resource "oci_containerengine_node_pool" "k8s_arm_node_pool" {
    cluster_id         = oci_containerengine_cluster.k8s_cluster.id
    compartment_id     = var.compartment_ocid
    kubernetes_version = var.k8s_version
    name               = "oci-k8s-node-pool-arm"
    node_config_details {
        placement_configs {
            availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
            subnet_id           = oci_core_subnet.vcn_node_subnet.id
        }
        placement_configs {
            availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
            subnet_id           = oci_core_subnet.vcn_node_subnet.id
        }
        size = 2

        nsg_ids = [oci_core_network_security_group.k8s_nodes.id]
    }
    node_shape = var.arm_node_shape # always-free ARM
    node_shape_config {
        memory_in_gbs = 12
        ocpus         = 2
    }
    node_source_details {
        image_id    = data.oci_core_images.arm_image.images[0].id
        source_type = "image"
    }
    initial_node_labels {
        key   = "name"
        value = "oci-cluster"
    }
    ssh_public_key = data.local_file.ssh_key.content
}
